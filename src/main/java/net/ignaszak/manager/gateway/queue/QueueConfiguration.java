package net.ignaszak.manager.gateway.queue;

import lombok.AllArgsConstructor;
import net.ignaszak.manager.commons.conf.properties.ManagerProperties;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AllArgsConstructor
public class QueueConfiguration {

    private static final String GATEWAY = "gateway";

    private final ManagerProperties managerProperties;

    @Bean
    public Queue gatewayQueue() {
        return new Queue(managerProperties.getJms().getQueues().get(GATEWAY).getQueue(), false);
    }

    @Bean
    public TopicExchange messagesGatewayExchange() {
        return new TopicExchange(managerProperties.getJms().getQueues().get(GATEWAY).getExchange());
    }

    @Bean
    public Binding gatewayQueueToExchangeBinding(Queue queue, TopicExchange exchange) {
        return BindingBuilder
                .bind(queue)
                .to(exchange)
                .with(managerProperties.getJms().getQueues().get(GATEWAY).getRoutingKey());
    }
}
