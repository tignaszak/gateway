package net.ignaszak.manager.gateway.queue;

import lombok.AllArgsConstructor;
import net.ignaszak.manager.commons.messaging.gateway.GatewayMappingMessage;
import net.ignaszak.manager.gateway.mapping.MappingServiceInterface;
import net.ignaszak.manager.gateway.mapping.mapper.MappingMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class MappingConsumer {

    private final MappingMapper mapper;
    private final MappingServiceInterface mappingService;

    @RabbitListener(queues = {"#{@gatewayQueue.name}"})
    public void receiveMessage(@Payload GatewayMappingMessage message) {
        final var mappingTOSet = mapper.mappingSetToMappingTOSet(message.getMappings());
        mappingService.register(mappingTOSet);
    }
}
