package net.ignaszak.manager.gateway.to;

public record MappingTO(String path, String type) {}
