package net.ignaszak.manager.gateway.tokens;

public class TokensConnectorException extends Exception {
    public TokensConnectorException(String message) {
        super(message);
    }
}
