package net.ignaszak.manager.gateway.tokens;

import net.ignaszak.manager.commons.conf.properties.ManagerProperties;
import net.ignaszak.manager.commons.httpclient.Request;
import net.ignaszak.manager.commons.httpclient.Response;
import net.ignaszak.manager.commons.httpclient.client.HttpClient;
import net.ignaszak.manager.commons.rest.response.ErrorInfoResponse;
import net.ignaszak.manager.commons.rest.response.ErrorResponse;
import net.ignaszak.manager.commons.rest.token.TokenValidationRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import static org.apache.commons.lang.StringUtils.isBlank;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@Component
public class TokensConnectorImpl implements TokensConnector {

    private static final String TOKEN_VALIDATE_URL = "/v1/tokens/api/auth/validate";

    private final HttpClient httpClient;
    private final String gatewayUri;

    TokensConnectorImpl(final HttpClient httpClient, final ManagerProperties managerProperties) throws TokensConnectorException {
        if (StringUtils.isBlank(managerProperties.getServer().getGatewayUri())) {
            throw new TokensConnectorException("Invalid gateway uri!");
        }

        this.httpClient = httpClient;
        this.gatewayUri = managerProperties.getServer().getGatewayUri();
    }

    @Override
    public void validate(String jwt) throws TokensConnectorException {
        var response = getTokenValidationResponse(jwt);

        if (NO_CONTENT != response.getStatus()) {
            throw new TokensConnectorException("Could not validate jwt token.");
        }
    }

    @Override
    @SuppressWarnings("squid:S3655")
    public void validateInactive(String jwt) throws TokensConnectorException {
        var response = getTokenValidationResponse(jwt);

        if (NO_CONTENT != response.getStatus()) {
            boolean isInactive = false;

            if (response.getBody().isPresent() && response.getBody().get() instanceof ErrorResponse errorResponse) {
                isInactive = errorResponse.getErrors()
                        .stream()
                        .map(ErrorInfoResponse::getCode)
                        .allMatch("TOK-3"::equals);
            }

            if (!isInactive) {
                throw new TokensConnectorException("Could not validate jwt token.");
            }
        }
    }

    private Response getTokenValidationResponse(final String jwt) throws TokensConnectorException {
        if (isBlank(jwt)) {
            throw new TokensConnectorException("Invalid jwt token!");
        }

        var validateRequest = new TokenValidationRequest(jwt);

        var request = Request.withUrl(gatewayUri + TOKEN_VALIDATE_URL)
                .body(validateRequest)
                .build();

        return httpClient.send(request);
    }
}
