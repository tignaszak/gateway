package net.ignaszak.manager.gateway.tokens;

public interface TokensConnector {
    void validate(String jwt) throws TokensConnectorException;
    void validateInactive(String jwt) throws TokensConnectorException;
}
