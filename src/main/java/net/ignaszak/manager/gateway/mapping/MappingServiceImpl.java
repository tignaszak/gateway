package net.ignaszak.manager.gateway.mapping;

import lombok.extern.log4j.Log4j2;
import net.ignaszak.manager.commons.spring.handler.repository.Mapping;
import net.ignaszak.manager.commons.spring.handler.repository.Mapping.Type;
import net.ignaszak.manager.commons.spring.handler.repository.RequestMappingRepository;
import net.ignaszak.manager.commons.spring.handler.repository.RequestMappingRepositorySingleton;
import net.ignaszak.manager.gateway.to.MappingTO;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@Log4j2
public class MappingServiceImpl implements MappingServiceInterface {

    private static final RequestMappingRepository MAPPING_REPOSITORY = RequestMappingRepositorySingleton.INSTANCE;

    @Override
    public void register(Set<MappingTO> mappings) {
        for (var mappingTO : mappings) {
            try {
                final var mapping = new Mapping(mappingTO.path(), Type.valueOf(mappingTO.type()));

                MAPPING_REPOSITORY.add(mapping);

                log.info("Registered request mapping: {}", mapping);
            } catch (IllegalArgumentException e) {
                log.error("Could not register request mapping: {}", mappingTO);
            }
        }
    }
}
