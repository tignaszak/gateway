package net.ignaszak.manager.gateway.mapping.mapper;

import net.ignaszak.manager.commons.messaging.gateway.GatewayMappingMessage;
import net.ignaszak.manager.gateway.to.MappingTO;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
@Mapper(componentModel = "spring")
public interface MappingMapper {
    Set<MappingTO> mappingSetToMappingTOSet(Set<GatewayMappingMessage.Mapping> mappings);
}
