package net.ignaszak.manager.gateway.mapping;

import net.ignaszak.manager.gateway.to.MappingTO;

import java.util.Set;

public interface MappingServiceInterface {
    void register(Set<MappingTO> mappings);
}
