package net.ignaszak.manager.gateway.security.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.ignaszak.manager.commons.error.Error;

@Getter
@AllArgsConstructor
public enum GatewayError implements Error {

    GAT_SERVICE_UNAVAILABLE("GAT-1", "Requested service is unavailable!");

    String code;
    String message;

    @Override
    public String getName() {
        return name();
    }

    @Override
    public String toString() {
        return getString();
    }
}
