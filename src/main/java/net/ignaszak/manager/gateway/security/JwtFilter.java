package net.ignaszak.manager.gateway.security;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import net.ignaszak.manager.commons.rest.response.ErrorResponse;
import net.ignaszak.manager.commons.spring.handler.repository.Mapping;
import net.ignaszak.manager.commons.spring.handler.repository.RequestMappingRepositorySingleton;
import net.ignaszak.manager.commons.spring.security.matcher.UriMatcher;
import net.ignaszak.manager.commons.spring.security.matcher.UriMatcherImpl;
import net.ignaszak.manager.gateway.tokens.TokensConnector;
import net.ignaszak.manager.gateway.tokens.TokensConnectorException;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

import static net.ignaszak.manager.commons.error.AuthError.AUT_INVALID_TOKEN;
import static net.ignaszak.manager.commons.jwt.JwtConstants.TOKEN_HEADER;
import static net.ignaszak.manager.commons.spring.handler.repository.Mapping.Type.ALLOWED;
import static net.ignaszak.manager.commons.spring.handler.repository.Mapping.Type.AUTHENTICATED_INACTIVE;
import static net.ignaszak.manager.commons.utils.JsonUtils.toJson;

@Log4j2
@Component
@AllArgsConstructor
public class JwtFilter implements WebFilter {

    private static final String API_DOCS_URL = "/v\\d+/[a-z]+/api-docs";
    private static final String SWAGGER_UI = "/swagger-ui.html";
    private static final String SWAGGER_UI_ASSETS = "/webjars/swagger-ui/**";
    private static final String SWAGGER_API_DOC = "/api-docs";
    private static final String SWAGGER_API_DOCS = "/api-docs/**";

    private static final UriMatcher URI_MATCHER;

    private static final DataBufferFactory DATA_BUFFER_FACTORY = new DefaultDataBufferFactory();

    static {
        final var uriRepository = RequestMappingRepositorySingleton.INSTANCE;

        uriRepository.add(new Mapping(API_DOCS_URL, ALLOWED));

        uriRepository.add(new Mapping(SWAGGER_API_DOCS, ALLOWED));
        uriRepository.add(new Mapping(SWAGGER_API_DOC, ALLOWED));
        uriRepository.add(new Mapping(SWAGGER_UI, ALLOWED));
        uriRepository.add(new Mapping(SWAGGER_UI_ASSETS, ALLOWED));

        URI_MATCHER = new UriMatcherImpl(uriRepository);
    }

    private final TokensConnector tokensConnector;

    @Override
    public Mono<Void> filter(final ServerWebExchange serverWebExchange, final WebFilterChain webFilterChain) {
        final var request = serverWebExchange.getRequest();
        final var requestUri = request.getURI().getPath();

        Mono<Void> mono;

        final var mappingType = URI_MATCHER.match(requestUri).getType();

        if (ALLOWED != mappingType) {
            try {
                final var token = request.getHeaders().getFirst(TOKEN_HEADER);

                if (AUTHENTICATED_INACTIVE == mappingType) {
                    tokensConnector.validateInactive(token);
                } else {
                    tokensConnector.validate(token);
                }

                mono = webFilterChain.filter(serverWebExchange);
            } catch (TokensConnectorException e) {
                log.error("Could not validate jwt token", e);

                final var errorResponse = ErrorResponse.of(AUT_INVALID_TOKEN.getCode(), AUT_INVALID_TOKEN.getMessage());

                final var dataBuffer = DATA_BUFFER_FACTORY.wrap(toJson(errorResponse).getBytes());

                serverWebExchange.getResponse().setStatusCode(HttpStatus.FORBIDDEN);
                mono = serverWebExchange.getResponse().writeWith(Mono.just(dataBuffer));
            }
        } else {
            mono = webFilterChain.filter(serverWebExchange);
        }

        return mono;
    }
}
