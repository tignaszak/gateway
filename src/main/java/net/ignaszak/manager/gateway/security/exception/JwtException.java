package net.ignaszak.manager.gateway.security.exception;

import net.ignaszak.manager.commons.error.AuthError;
import net.ignaszak.manager.commons.error.exception.ErrorException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class JwtException extends ErrorException {
    public JwtException() {
        super(AuthError.AUT_INVALID_TOKEN);
    }
}
