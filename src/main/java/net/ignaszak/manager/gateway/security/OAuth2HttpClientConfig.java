package net.ignaszak.manager.gateway.security;

import net.ignaszak.manager.commons.conf.properties.ManagerProperties;
import net.ignaszak.manager.commons.httpclient.client.HttpClient;
import net.ignaszak.manager.commons.httpclient.client.OAuth2HttpClient;
import net.ignaszak.manager.commons.httpclient.client.provider.Oauth2ProviderBuilder;
import net.ignaszak.manager.commons.httpclient.registry.ClientRegistrationFactory;
import net.ignaszak.manager.commons.spring.config.CommonOAuth2ClientConfig;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.web.client.RestTemplate;

@Configuration
@Import(CommonOAuth2ClientConfig.class)
@EnableConfigurationProperties(ManagerProperties.class)
@ComponentScan("net.ignaszak.manager.commons.conf")
public class OAuth2HttpClientConfig {

    @Bean
    public ClientRegistrationRepository clientRegistrationRepository(ManagerProperties managerProperties) {
        var factory = new ClientRegistrationFactory(
                managerProperties.getServer().getGatewayUri() + managerProperties.getOAuth2().getTokenEndpoint(),
                managerProperties.getOAuth2().getClients()
        );

        return new InMemoryClientRegistrationRepository(factory.getClientRegistrationList());
    }

    @Bean
    public HttpClient oAuth2TokensHttpClient(Oauth2ProviderBuilder oauth2ProviderBuilder) {
        var restTemplate = new RestTemplate();
        var oAuth2Provider = oauth2ProviderBuilder
                .withClientRegistrationId("tokens")
                .build();

        return new OAuth2HttpClient(restTemplate, oAuth2Provider);
    }
}
