package net.ignaszak.manager.gateway.integration

import com.github.tomakehurst.wiremock.WireMockServer
import net.ignaszak.manager.commons.messaging.gateway.GatewayMappingMessage
import net.ignaszak.manager.commons.rest.response.ErrorResponse
import net.ignaszak.manager.commons.test.specification.IntegrationSpecification
import net.ignaszak.manager.commons.utils.JsonUtils
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource

import java.util.stream.Collectors

import static com.github.tomakehurst.wiremock.client.WireMock.get
import static com.github.tomakehurst.wiremock.client.WireMock.noContent
import static com.github.tomakehurst.wiremock.client.WireMock.ok
import static com.github.tomakehurst.wiremock.client.WireMock.post
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo
import static net.ignaszak.manager.commons.error.AuthError.AUT_INVALID_TOKEN
import static org.springframework.http.HttpStatus.FORBIDDEN
import static org.springframework.http.HttpStatus.OK
import static org.springframework.http.MediaType.APPLICATION_JSON

class GatewayIntegrationSpec extends IntegrationSpecification {

    private static final int MOCK_TOKENS_SERVER_PORT = 8111
    private static final int MOCK_USERS_SERVER_PORT = 8112

    private static final String SUCCESS_RESPONSE = "{\"response\":\"SUCCESS\"}"

    @DynamicPropertySource
    private static void postgresqlProperties(DynamicPropertyRegistry registry) {
        registry.add("manager.server.gatewayUri", () -> "http://localhost:${MOCK_TOKENS_SERVER_PORT}")

        registry.add("spring.cloud.gateway.routes[0].id", () -> "test-service")
        registry.add("spring.cloud.gateway.routes[0].uri", () -> "http://localhost:${MOCK_USERS_SERVER_PORT}")
        registry.add("spring.cloud.gateway.routes[0].predicates[0]", () -> "Path=/v1/service/**")
        registry.add("spring.cloud.gateway.routes[0].filters[0]", () -> "RewritePath=/v1/service(?<segment>/?.*), /\$\\{segment}")
    }

    @Autowired
    private RabbitTemplate rabbitTemplate

    def "successful redirect with authenticated route"() {
        setup:
        def mockTokensServer = initMockTokensServer()
        mockTokensServer.stubFor(post(urlEqualTo("/v1/tokens/api/auth/validate")).willReturn(noContent()))
        def mockService = initMockServiceServer()

        when:
        def responseEntity = restTemplateAuthFacade.get("/v1/service/test") as ResponseEntity<Map<String, String>>

        then:
        responseEntity.statusCode == OK
        responseEntity.body.response == "SUCCESS"

        cleanup:
        mockTokensServer.stop()
        mockService.stop()
    }

    def "failed redirect with authenticated route"() {
        setup:
        def mockTokensServer = initMockTokensServer()
        mockTokensServer.stubFor(post(urlEqualTo("/v1/tokens/api/auth/validate")).willReturn(noContent()))
        def mockService = initMockServiceServer()

        when:
        def responseEntity = restTemplate.getForEntity("http://localhost:${port}/v1/service/test", String)

        then:
        responseEntity.statusCode == FORBIDDEN
        assertErrorMessage(responseEntity, AUT_INVALID_TOKEN)

        cleanup:
        mockTokensServer.stop()
        mockService.stop()
    }

    def "successful redirect with unauthenticated route"() {
        setup:
        def mockTokensServer = initMockTokensServer()
        mockTokensServer.stubFor(post(urlEqualTo("/v1/tokens/api/auth/validate")).willReturn(noContent()))
        def mockService = initMockServiceServer()
        sendAllowedMapping()

        when:
        def responseEntity = restTemplateAuthFacade.get("/v1/service/test") as ResponseEntity<Map<String, String>>

        then:
        responseEntity.statusCode == OK
        responseEntity.body.response == "SUCCESS"

        cleanup:
        mockTokensServer.stop()
        mockService.stop()
    }

    private void sendAllowedMapping() {
        def gatewayMessage = new GatewayMappingMessage(
                Set.of(
                        new GatewayMappingMessage.Mapping("/v1/service/test", "ALLOWED")
                )
        )

        rabbitTemplate.convertAndSend(
                managerProperties.jms.queues.gateway.exchange,
                managerProperties.jms.queues.gateway.routingKey,
                gatewayMessage
        )
    }

    private static void assertErrorMessage(ResponseEntity<String> responseEntity, net.ignaszak.manager.commons.error.Error error) {
        def errorResponse = JsonUtils.toObject(responseEntity.body, ErrorResponse)
        errorResponse.errors
                .stream()
                .map(e -> e.code)
                .collect(Collectors.toSet())
                .contains(error.code)
    }

    private static WireMockServer initMockTokensServer() {
        def mockServer = new WireMockServer(MOCK_TOKENS_SERVER_PORT)
        mockServer.start()

        mockServer.stubFor(
                post(urlEqualTo("/v1/tokens/oauth2/token"))
                        .willReturn(ok().withHeader("Content-Type", APPLICATION_JSON.toString()).withBody(anyOauthTokenResponse()))
        )

        mockServer
    }

    private static WireMockServer initMockServiceServer() {
        def mockServer = new WireMockServer(MOCK_USERS_SERVER_PORT)
        mockServer.start()
        mockServer.stubFor(get(urlEqualTo("/test")).willReturn(
                ok()
                        .withHeader("Content-Type", APPLICATION_JSON.toString())
                        .withBody(SUCCESS_RESPONSE))
        )
        mockServer
    }

    private static String anyOauthTokenResponse() {
        """
        {
          "access_token": "access token code",
          "scope": "validate register",
          "token_type": "Bearer",
          "expires_in": 299
        }
        """
    }
}
