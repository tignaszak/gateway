package net.ignaszak.manager.gateway.security

import net.ignaszak.manager.commons.spring.handler.repository.Mapping
import net.ignaszak.manager.commons.spring.handler.repository.RequestMappingRepositorySingleton
import net.ignaszak.manager.gateway.tokens.TokensConnector
import org.junit.jupiter.api.extension.ExtendWith
import org.spockframework.spring.SpringBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.http.HttpCookie
import org.springframework.http.HttpHeaders
import org.springframework.http.server.RequestPath
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.util.MultiValueMap
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilterChain
import reactor.core.publisher.Flux
import spock.lang.Specification

import static net.ignaszak.manager.commons.jwt.JwtConstants.TOKEN_HEADER
import static net.ignaszak.manager.commons.jwt.JwtConstants.TOKEN_PREFIX
import static net.ignaszak.manager.commons.spring.handler.repository.Mapping.Type.AUTHENTICATED_INACTIVE

@WebFluxTest
@ExtendWith(SpringExtension)
class JwtFilterSpec extends Specification {

    @SpringBean
    private TokensConnector tokensConnectorMock = Mock()

    private ServerWebExchange serverWebExchangeMock = Mock()

    private WebFilterChain webFilterChainMock = Mock()

    private JwtFilter subject = new JwtFilter(tokensConnectorMock)

    @Autowired
    private WebTestClient webClient

    def "should not access authenticated url without token"() {
        when:
        def response = webClient.post().uri("/any/authenticated/url").exchange()

        then:
        response.expectStatus().isForbidden()
    }

    def "should access auth endpoints without token with any versions"(String url) {
        when:
        webClient.post().uri(url).exchange()

        then:
        0 * tokensConnectorMock.validate(any())

        where:
        url                             | _
        "/v1/users/auth/login"          | _
        "/v111/users/auth/login"        | _
        "/v1/users/auth/registration"   | _
        "/v123/users/auth/registration" | _
    }

    def "should filter allowed request without token" () {
        given:
        1 * serverWebExchangeMock.getRequest() >> anyRequest("/swagger-ui.html")

        when:
        subject.filter(serverWebExchangeMock, webFilterChainMock)

        then:
        0 * tokensConnectorMock.validate(_)
        0 * tokensConnectorMock.validateInactive(_)
    }

    def "should filter authenticated user token" () {
        given:
        1 * serverWebExchangeMock.getRequest() >> anyRequest("/v1/users/me")

        when:
        subject.filter(serverWebExchangeMock, webFilterChainMock)

        then:
        1 * tokensConnectorMock.validate(anyJwtToken())
    }

    def "should filter authenticated inactive user token" () {
        given:
        def path = "/v1/users/api/auth/confirm_email/1234567890"
        RequestMappingRepositorySingleton.INSTANCE.add(new Mapping(path, AUTHENTICATED_INACTIVE))
        1 * serverWebExchangeMock.getRequest() >> anyRequest(path)

        when:
        subject.filter(serverWebExchangeMock, webFilterChainMock)

        then:
        1 * tokensConnectorMock.validateInactive(anyJwtToken())
    }

    private static ServerHttpRequest anyRequest(String request) {
        new ServerHttpRequest() {
            @Override
            String getId() {
                return null
            }

            @Override
            RequestPath getPath() {
                return null
            }

            @Override
            MultiValueMap<String, String> getQueryParams() {
                return null
            }

            @Override
            MultiValueMap<String, HttpCookie> getCookies() {
                return null
            }

            @Override
            String getMethodValue() {
                return null
            }

            @Override
            URI getURI() {
                return new URI(request)
            }

            @Override
            HttpHeaders getHeaders() {
                return new HttpHeaders(Map.of(TOKEN_HEADER, anyJwtToken()))
            }

            @Override
            Flux<DataBuffer> getBody() {
                return null
            }
        }
    }

    private static String anyJwtToken() {
        TOKEN_PREFIX + "123"
    }
}
