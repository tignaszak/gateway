package net.ignaszak.manager.gateway.tokens

import net.ignaszak.manager.commons.conf.properties.ManagerProperties
import net.ignaszak.manager.commons.httpclient.Response
import net.ignaszak.manager.commons.httpclient.client.HttpClient
import net.ignaszak.manager.commons.rest.response.ErrorInfoResponse
import net.ignaszak.manager.commons.rest.response.ErrorResponse
import net.ignaszak.manager.commons.test.ManagerPropertiesTest
import spock.lang.Specification

import static org.springframework.http.HttpStatus.BAD_REQUEST
import static org.springframework.http.HttpStatus.NO_CONTENT

class TokensConnectorSpec extends Specification {

    private HttpClient httpClientMock = Mock()
    private ManagerProperties managerProperties = ManagerPropertiesTest.managerProperties

    private TokensConnector subject = new TokensConnectorImpl(httpClientMock, managerProperties)

    def "no exception is thrown on success jwt validation" () {
        given:
        1 * httpClientMock.send(_) >> new Response(Optional.empty(), NO_CONTENT)

        when:
        subject.validate(anyJwt())

        then:
        noExceptionThrown()
    }

    def "no exception is thrown on success jwt validation when token is inactive" () {
        given:
        1 * httpClientMock.send(_) >> new Response(Optional.of(inactiveTokenErrorResponse()), BAD_REQUEST)

        when:
        subject.validateInactive(anyJwt())

        then:
        noExceptionThrown()
    }

    def "an exception is thrown when jwt token is invalid" () {
        given:
        def invalidJwt = ""

        when:
        subject.validate(invalidJwt)

        then:
        0 * httpClientMock.send(_)
        def e = thrown(TokensConnectorException)
        e.message == "Invalid jwt token!"
    }

    def "an exception is thrown when jwt is not validated" () {
        given:
        1 * httpClientMock.send(_) >> new Response(Optional.empty(), BAD_REQUEST)

        when:
        subject.validate(anyJwt())

        then:
        def e = thrown(TokensConnectorException)
        e.message.startsWith("Could not validate jwt token.")
    }

    private static String anyJwt() {
        "any jwt"
    }

    private static ErrorResponse inactiveTokenErrorResponse() {
        new ErrorResponse(Set.of(new ErrorInfoResponse("TOK-3", "Inactive token!")))
    }
}
