FROM openjdk:19-alpine
ADD target/*.jar manager_gateway_app.jar
ENTRYPOINT ["java", "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/manager_gateway_app.jar"]
EXPOSE 8080
EXPOSE 5005